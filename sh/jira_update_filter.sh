#
# update the JQL text of an existing filter,
# identified by $1,
# with text in $2
#
# note that the name attribute of that filter is NOT a required field 
# which is reasonable but differs from documentation at:
# https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-filters/#api-rest-api-3-filter-id-put
#

INSTANCE="$(sense-web-instance.sh)"

curl $CURLOPTS $( jira_credentials.sh ) -X PUT -H 'Content-Type: application/json' \
https://$INSTANCE/rest/api/3/filter/$1 \
--data "{  \"jql\" : \"$2\"  }"
