#
# run extraction and transformation in a sequence
# or in a loop if a parameter for sleep time is passed
#
# licensed under AGPLv3
# by Boran Gögetap
#

while true
do

 #
 # SETUP: any script in local folder with name 0-*.sh is executed before 1-extract script
 #
 if [ -f    0-*.sh ] ; then
  for  f in 0-*.sh   ; do
   source "$f"
  done
 fi # 0- scripts

 #
 # DEPEND: execute dependencies and record whether those produced newer results than anything the current dir
 #
 if test -f            dependencies.txt; then

  >&2 echo -e "\033[1;33mupdating dependencies...\033[0m"
         dependencies=$(<dependencies.txt)
  pushd .. >/dev/null
  echo "$dependencies"  | while read in; do cd12.sh $in/; done
  >&2 echo -e "\033[1;33mdone dependencies.\033[0m"
  popd >/dev/null

  while read in; do 
   if [ ../$in/issue-lines.json -nt . ]; then
    newerdependencies=true
   fi
  done < dependencies.txt 

 fi

 #
 # EXTRACT: run default 1-extract.sh or a 1-extract.sh script that is optoinally supplied in view dir INSTEAD
 #
 #    NOTE: this script shall indicate success with exit code of 0 or failure otherwise to prevent further processing
 #
  if [ -f   1-extract.sh ] ; then
   source ./1-extract.sh # run local script
  else
   1-extract.sh		 # run script from KTS path
  fi # 1-extract.sh

 if [ $? -eq 0 ] || [ $newerdependencies ] ; then


  #
  # AUTO-SUM
  #
  update-autosum-field.sh

  #
  # ENHANCE: any script in view dir with name 1-*.sh (except for 1-extract.sh) is executed after 1-extract but before the 2-*.sh scripts
  #
  find -name "1-*.sh" -not -name "1-extract.sh" -exec bash {} \;

  #
  # TRANSFORM: any script in view dir with name 2-*.sh is executed IN STEAD OF of 2-issues2dot.sh script
  #
  if [ -f    2-*.sh ] ; then
   find -name "2-*.sh" -exec bash {} \;
  else
   2-issues2dot.sh
  fi # 2- scripts
 
  #
  # DEPLOY and so on: any script in local folder with name 3-*.sh is executed after the 2-*.sh scripts
  #
  find -name "3-*.sh" -exec bash {} \;


 fi # successful 1-extract.sh

 #
 # CLEANUP
 #
 9-clean-2-temp.sh

 if test -z "$1"; then
  break
 else
  sleep $1
 fi

done
