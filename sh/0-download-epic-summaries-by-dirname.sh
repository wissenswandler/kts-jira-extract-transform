linesFilename=epic-lines.json
rm -f      $linesFilename
touch      $linesFilename
echo "1" >       total.json
startAt="0"
fields="summary,$(<customfield_epiccolor.txt),$(<customfield_epicname.txt)"
jql="type=Epic"
while test $( wc -l < $linesFilename ) -lt $(<total.json)
do
 jiracurl.sh "search?jql=$jql&fields=$fields&maxResults=100&startAt=$startAt" > results.json
 jq    < results.json " .total "    >  total.json
 jq -c < results.json " .issues[]"  >> $linesFilename
 startAt=$[$startAt+100]
done
rm   results.json
rm   total.json

jq       < $linesFilename "{ key, name : .fields.$(<customfield_epicname.txt) , summary : .fields.summary , color : .fields.$(<customfield_epiccolor.txt) }" > epic-summary-by-key.json
rm -f      $linesFilename
