if [ -z $1 ] ; then 
 echo Error: missing destination filepath
 exit 500
fi

jq -r .key issue-lines.json | while read key ; do save_line-issue_as_chunk.sh $key $1 ; done
