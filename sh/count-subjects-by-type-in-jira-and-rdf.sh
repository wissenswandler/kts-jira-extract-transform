echo -n "in RDF  = "
rdfcurl.sh "select (COUNT(*) AS ?count) {?s a/rdfs:label \"$1\"}" | jq -r .count

echo -n "in Jira = "
jiracurl.sh "search?jql=type=$1&maxResults=0" | jq .total
