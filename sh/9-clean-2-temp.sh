find -cnewer spo--full--lines.json \( -name 'graph*.dot' -o -name 'nodes-*.json' \) -delete 2>/dev/null

rm -f  spo--*.json

# rm -f nodes-*.json
# rm -f graph-t.dot

#
# note that graph.dot could be both an intermediate file (produced from issue-lines)
# or a manually written source
# therefore we remove it here only if newer than spo--full--lines.json
#
