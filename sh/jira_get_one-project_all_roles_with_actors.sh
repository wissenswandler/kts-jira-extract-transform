jiracurl.sh project/$1/role \
| \
jq -r "to_entries[]|.value" \
| \
while read -r line; do
 jiracurl.sh $line | jq 'select(.actors|length>0)'
done	# one project
