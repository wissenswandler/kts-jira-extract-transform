#
# jqi = jq-RDF-inspector, prints JSON for json-rdf by replacing type-value-objects simply with their value's value
#

jq -S '.results.bindings[]|walk( if type=="object" and .type and .value then .value else . end)' $*
