if [ -f ../autosum-field.txt ] && [ -f issue-lines.json ] ;
then

echo "calculating autosum..."

autoSum=$( jq -s "map( .fields.$(<../autosum-field.txt) )|add" issue-lines.json )
echo $autoSum  >     "autosum--$(<../autosum-field.txt).out"
echo $autoSum

list-matching-meta-spaces.sh | while read line
do
 echo updating $line ...
 jira_set_field.sh         $line $(<../autosum-field.txt) \
     $autoSum
 echo ... done.
done

fi # end if files exist
