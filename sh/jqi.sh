#
# jqi = jq-issue-inspector, prints JSON for Jira Issue removing some empty or secondary keys
#

jq -S "del( .fields[]|nulls ) | del( .fields[]|select( . == [] ) ) | del(..|.comment?) | del(..|.aggregateprogress?) | del(..|.avatarUrls?) | del(..|.self?) | del(..|.iconUrl?)" $@
