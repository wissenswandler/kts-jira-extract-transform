#
# get JQL definition from a Jira shared filter which is referenced in local jql.txt file
#

jiracurl.sh filter/$( cat jql.txt | sed -nr 's/filter=([0-9]*)$/\1/p') | jq -r .jql
