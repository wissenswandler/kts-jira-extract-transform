if [ -z $3 ] ; then

 echo "Usage: set value $3 of field $2 in issue $1"
 echo
 echo " use literal string '"null"' (without quotes) to clear a field"
 echo " otherwise the JSON representation of field type, i.e. literal number, double quotes for string, array [...] or object notation { ... }"
 echo
 echo " mind shell expansion for double quotes to reach the inside of this program, i.e. use a syntax as follows:"
 echo
 echo "  jira_set_field.sh XXX-258 summary '"'"Test1532"'"'"

 exit 64
fi

INSTANCE="$(sense-web-instance.sh)"

curl $CURLOPTS $( jira_credentials.sh ) -X PUT -H 'Content-Type: application/json' \
https://$INSTANCE/rest/api/3/issue/$1 \
--data "{  \"fields\" : { \"$2\" : $3 }  }"
