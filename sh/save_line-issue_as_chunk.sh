#
# quick note how to create an artificial webchunk
# (a file that functions as a webhook payload but did not actually arrive via webhook)
#

if [ -z $2 ] ; then 
 echo Error: missing destination filepath
 exit 500
fi

webchunks_issue_filepath_=$2

chunkfilename=$( mktemp )

#
# 1. isolate the issue that we are interested in
#
jq -c "select( .key == \"$1\" )" issue-lines.json > $chunkfilename
chmod +r $chunkfilename
issue_id=$( jq -r ".id" $chunkfilename )

if [ -z "$issue_id" ] ; then
 echo Error: no issue with key $1 in issue-lines.json
 exit 404
fi

destination=$webchunks_issue_filepath_$issue_id 

if [ -e $destination ]
then
  if [ $destination -nt issue-lines.json ] ; then
    echo skipping destination chunk $1 '->' $issue_id which is newer than issue-lines.json
    exit 304
  else
    timestamp_destination=$( jq -r '.fields.updated' $destination   )
    timestamp_source=$( jq -r '.fields.updated' $chunkfilename )
    if [ "$timestamp_source" == "null" ] ; then
      echo skipping source chunk $1 without timestamp
      exit 304
    fi
    if [ "$timestamp_destination" != "null" -a ! "$timestamp_source" \> "$timestamp_destination" ] ; then
      echo skipping source chunk $1, timestamped $timestamp_source, which is not reliably newer than $timestamp_destination
      exit 304
    fi
  fi
fi # destination chunk already exists

#
# 2. save JSON chunk as 'id'-based filename
#
echo saving $1 '->' $issue_id in $webchunks_issue_filepath_
jq "select( .id == \"$issue_id\" )" issue-lines.json > $chunkfilename


#
# 3. move chunk into webhook folder
#
mv $chunkfilename $destination

#
# 4. symlink with 'key'
#
if [ ! -e $webchunks_issue_filepath_$1 ] ; then
 ln -s $issue_id $webchunks_issue_filepath_$1
fi
