if [ -z $1 ] ; then
 echo -e "\033[1;31m" "Usage: jira_create_leb.sh AUFTRAGS-KEY" "\033[0m"
 exit 1
fi

export CURLOPTS=-s

INSTANCE="$(sense-web-instance.sh)"

jiracurl.sh issue/$1 | jq -S > auftrag.json

TEMPLATE=$( jq -r .fields.$(< ../customfield_spacetemplate.txt ).key auftrag.json )
if [ $TEMPLATE == 'null' ] ; then
 echo -e "\033[1;31m" "Error: $1 hat keinen Bereich (als Vorlage für neuen LEB) definiert." "\033[0m"
 exit 2
else
 echo -e "Bereichsvorlage \033[1;33m" $TEMPLATE "\033[0m"
fi

jiracurl.sh project/$TEMPLATE					| jq -S				 > template.json
jiracurl.sh project/$TEMPLATE/notificationscheme		| jq -S				 > notischm.json
jiracurl.sh project/$TEMPLATE/permissionscheme			| jq -S				 > permschm.json
jiracurl.sh project/VORLAGELEISTERB/issuesecuritylevelscheme	| jq -S				 > isecschm.json
# workflowscheme API does not accept project KEY
jiracurl.sh "workflowscheme/project?projectId=$( jq -r .id template.json )" \
								| jq -S .values[].workflowScheme > workschm.json
./calculate_create_space_data.sh > space_data.json

if [ $? != 0 ] ; then
 echo -e "\033[1;31m" stop "\033[0m"
else

curl $CURLOPTS $( jira_credentials.sh ) -X POST -H 'Content-Type: application/json' \
 --url https://$INSTANCE/rest/api/3/project	\
 --data @space_data.json			\
 > space_receipt.json

if [ $? == 0 ] ; then
 echo -e "\033[1;32m" LEB erfolgreich erzeugt: "\033[0m"
 jq . space_receipt.json
 SPACEKEY=$( jq -r .key space_receipt.json )
 mkdir ../$SPACEKEY
 jira_set_field.sh $1 $(< ../customfield_spacetemplate.txt)	null
 jira_set_field.sh $1 $(< ../customfield_space.txt	)	$( jq -cM '{key}' space_receipt.json )

 # SYMSUITE-520 / see also SYMSUITE-521
 sleep 3 # must wait for RDF import for SPARQL query to succeed
 pushd ../SYMSUITE-521
 jira_set_group-actor_for_project-role.sh $SPACEKEY 10289 $( ./get_accessgroup_for_LEB.sh $SPACEKEY )
 popd

else
 echo -e "\033[1;31m" cURL error with payload: "\033[0m"
 jq . space_data.json
fi

fi # success of calculate_create_space_data

#
# for the records
#

mv space_receipt.json	space_receipt_for_$1.json
mv space_data.json	   space_data_for_$1.json
