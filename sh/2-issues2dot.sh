SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
KTSJET_HOME=$(dirname "$SCRIPTPATH")
KTSJET_JQ="$KTSJET_HOME/jq"

if [ "$1" == "" ]; then
 ISSUES_FILENAME=issue-lines.json
 GRAPHB_FILENAME=graph
else
 ISSUES_FILENAME=$1
 GRAPHB_FILENAME=$2
fi

if [ ! -f $ISSUES_FILENAME ] ; then echo -e "\033[1;31mno input file - aborting transformation\033[0m" ; exit 304 ; fi

if [ -z "$group_by" ] ; then
 group_by=true
fi
export group_by
echo "transforming JSON to DOT"
echo "grouped by: " $group_by

if [ ! -f title.txt ] ; then
 jira_get_project_title.sh
fi

if [ -f wantedEpics.txt ] ; then
 slurpWantedEpics='--slurpfile wantedEpics wantedEpics.txt'
else
 slurpWantedEpics='--argjson   wantedEpics []'
fi

if [ -f wantedLabels.txt ] ; then
 slurpWantedLabels='--slurpfile wantedLabels wantedLabels.txt'
else
 slurpWantedLabels='--argjson   wantedLabels []'
fi

if [ -f wantedTypenames.txt ] ; then
 slurpWantedTypenames='--slurpfile wantedTypenames wantedTypenames.txt'
else
 slurpWantedTypenames='--argjson   wantedTypenames []'
fi

if [ -f unwantedTypenames.txt ] ; then
 slurpUnwantedTypenames='--slurpfile unwantedTypenames unwantedTypenames.txt'
else
 slurpUnwantedTypenames='--argjson   unwantedTypenames []'
fi

if [ -f               show.json ] ; then
 show='--argfile show show.json'
else
 show="--argfile show $KTSJET_HOME/config/show-all.json"
fi

if [ -f                 includeLinkedNodes.txt ] ; then
 includeLinkedNodes=$(< includeLinkedNodes.txt )
else
 includeLinkedNodes=all
fi

#
# start this diagram's DOT file with all the existing headers
# or use the default KTS header
#
if compgen -G "0??-header*.dot" > /dev/null; then
 cat           0??-header*.dot  > ${GRAPHB_FILENAME}-t.dot
else
 cat  $KTSJET_HOME/defaults/000-header.dot > ${GRAPHB_FILENAME}-t.dot
fi

#
echo process edges into input model with all Jira information | ts '%H:%M:%S -'
#

jq     -L $KTSJET_JQ 'include "select-edges-from-issue-lines"; doit' \
	$ISSUES_FILENAME \
| \
jq -sc -L $KTSJET_JQ 'include "filter-edges--lines"; doit' \
\
>       spo--full--lines.json

#
echo simplify edges into minimal structural facts | ts '%H:%M:%S -'
#
jq  -c  "{ s : .s|{key} , p : .p|{name} , o : .o|{key} }" \
	spo--full--lines.json	\
>       spo--skey-pname-okey.json

#
echo process nodes into temp json | ts '%H:%M:%S -'
#
jq     -L $KTSJET_JQ 'include "select-nodes-and-linked-nodes"; doit'	\
--arg epicLinkField      $(<../customfield_epiclink.txt)		\
--arg includeLinkedNodes $includeLinkedNodes  		\
	$ISSUES_FILENAME	\
| \
jq -s  -L $KTSJET_JQ 'include "filter-nodes"; doit' \
	$slurpUnwantedTypenames \
	$slurpWantedTypenames   \
	$slurpWantedEpics       \
	$slurpWantedLabels      \
	--slurpfile spo		\
	spo--full--lines.json	\
>	nodes-2.json

#
# optional user-supplied filter nodes
#
if [ -f nodes-filter.jq ] ; then
 echo applying user-defined node filter | ts '%H:%M:%S -'
 jq  -f nodes-filter.jq		\
	nodes-2.json		\
 >	nodes-3.json
 echo $(jq length nodes-2.json) " -> " $(jq length nodes-3.json) 
else
 mv	nodes-2.json		\
 	nodes-3.json
fi

#
echo group nodes | ts '%H:%M:%S -'
# grouping; adds one array level:
# we get one array of grouped (possibly only one) arrays
#
jq " group_by( $group_by ) "	\
	nodes-3.json		\
>	nodes-4.json

#
echo render nodes to DOT | ts '%H:%M:%S -'
#
jq -r  -f $KTSJET_JQ/nodes-to-dot--raw.jq					\
        $show									\
	--slurpfile spo              spo--skey-pname-okey.json			\
	--slurpfile epicSummaryByKey ../epic-summary-by-key.json		\
	--slurpfile colors           $KTSJET_HOME/config/jira-colors.json	\
	nodes-4.json \
>>      ${GRAPHB_FILENAME}-t.dot

#
echo render edges to DOT | ts '%H:%M:%S -'
#
jq -sr -L $KTSJET_JQ 'include "edge-lines-to-dot--raw"; doit'	\
	$slurpUnwantedTypenames					\
	--slurpfile nodes nodes-4.json				\
	spo--full--lines.json					\
>>      ${GRAPHB_FILENAME}-t.dot

#
echo process optional virtual edges | ts '%H:%M:%S -'
#
if [ -f generate-virtual-edges.sh ] ; then
      ./generate-virtual-edges.sh		\
>>      ${GRAPHB_FILENAME}-t.dot
fi

#
echo render parental containments to DOT | ts '%H:%M:%S -'
# IF parent is part of this view
#
jq -rf $KTSJET_JQ/parent-lines-to-dot--raw.jq	\
	--slurpfile nodes nodes-4.json		\
	$ISSUES_FILENAME			\
>>      ${GRAPHB_FILENAME}-t.dot

#
# append epilogue content to DOT
#
[ -f 9??-epilogue*.dot ] && cat 9??-epilogue*.dot >> ${GRAPHB_FILENAME}-t.dot

#
echo close DOT | ts '%H:%M:%S -'
#
echo -e "\n}" >> ${GRAPHB_FILENAME}-t.dot

#
# on supported plattforms (with graphviz package installed): restructure DOT
#
hash unflatten 2>/dev/null && unflatten -l 5 -c 5  < ${GRAPHB_FILENAME}-t.dot > ${GRAPHB_FILENAME}.dot || mv ${GRAPHB_FILENAME}-t.dot ${GRAPHB_FILENAME}.dot

#
# on supported plattforms (with graphviz package installed): translate DOT into SVG, PDF and PNG
#
hash dot 2>/dev/null && ( \

echo rendering SVG | ts '%H:%M:%S -'
dot -Tsvg ${GRAPHB_FILENAME}.dot > ${GRAPHB_FILENAME}-local.svg
echo -n "postprocessing SVG " | ts '%H:%M:%S -'
inject-title-into-graph-svg.sh     ${GRAPHB_FILENAME}-local.svg
4-convert-avatars-back-to-site-urls.sh ${GRAPHB_FILENAME}-local.svg > ${GRAPHB_FILENAME}-navigation.svg
3-1-postprocess-svg.sh             ${GRAPHB_FILENAME}-local.svg

echo rendering PDF | ts '%H:%M:%S -'
dot -Tpdf ${GRAPHB_FILENAME}.dot > ${GRAPHB_FILENAME}-print.pdf

echo rendering PNG | ts '%H:%M:%S -'
dot -Tpng ${GRAPHB_FILENAME}.dot > ${GRAPHB_FILENAME}.png

#
# for performance: skipping the downscale
#
# TODO: add heuristics when to scale,
# OR add a tagging file to request thumbnails,
# AND/OR use graphviz's size parameters in order to render smaller PNGs (instead imagemagick's convert) 
#

#echo "..scaling down"
#convert                            ${GRAPHB_FILENAME}.png \
#                       -resize 50% ${GRAPHB_FILENAME}-overview.png
#convert                            ${GRAPHB_FILENAME}-overview.png \
#                       -resize 50% ${GRAPHB_FILENAME}-thumbnail.png 

)

#
# TODO: else (no graphviz) use viz.js (in preparation)
#

# 12.sh also cleans up, so we don't need to do it here
#
# 9-clean-2-temp.sh

echo -e "\033[1;32mKTS done.\033[0m" | ts '%H:%M:%S -'
