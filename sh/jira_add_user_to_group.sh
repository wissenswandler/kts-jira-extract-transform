#
# add user with accountId $1 to group with id $2
#
# with error handling based on HTTP return code: any HTTP return code < 200 or > 209 will cause unix exit code = 2
# so that this script can safely be chained with another command only in case of success
#
# potential error cases are:
#
# HTTP 400
# if user is already member of group
#
# HTTP 40r
# if user does not exist
#

INSTANCE="$(sense-web-instance.sh)"

OUTPUT_FILE=api-results.json

CURLOPTS="--silent --output $OUTPUT_FILE --write-out %{http_code}"

HTTP_CODE=$(  curl $CURLOPTS $( jira_credentials.sh ) -X POST -H 'Content-Type: application/json' \
"https://$INSTANCE/rest/api/3/group/user?groupId=$2" \
--data "{ \"accountId\": \"$1\"  }"  )

if [[ ${HTTP_CODE} -lt 200 || ${HTTP_CODE} -gt 299 ]] ; then
 >&2 echo "KTS reports: HTTP Status $HTTP_CODE (an error)"
 >&2 echo "API reports: "
 >&2 cat $OUTPUT_FILE
 >&2 echo
 exit 2
else
 jq . $OUTPUT_FILE
fi
