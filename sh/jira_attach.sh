INSTANCE="$(sense-web-instance.sh)"
  
  curl $CURLOPTS $( jira_credentials.sh ) -X POST -H "X-Atlassian-Token: no-check" -D- -F "file=@$2" https://$INSTANCE/rest/api/3/issue/$1/attachments

# curl $CURLOPTS $( jira_credentials.sh ) -X POST -H "Content-Type: application/json"                https://$INSTANCE/rest/api/3/issue/$1/comment \
# --data '{
#	   "body": {
#	     "type": "doc",
#	     "version": 1,
#	     "content": [ {
#	       "type": "paragraph",
#	       "content": [ {
#		 "type": "text" ,
#		 "text": "!graph.png|thumbnail!"
#		  } ]
#	     } ]
#	   }
#	}'
