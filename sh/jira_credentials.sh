DIRTHIS=$(  basename            "$(pwd)"    )
DIRSUPER=$( basename "$(dirname "$(pwd)" )" )

if   echo $DIRTHIS  | grep -qF "." ; then
 INSTANCE=$DIRTHIS
elif echo $DIRSUPER | grep -qF "." ; then
 INSTANCE=$DIRSUPER
 VIEW="--"$DIRTHIS
else
 echo "ERROR: unable to detect JIRA-INSTANCE from dotted directory (neither this nor super dir)" >&2
 echo " and exiting now." >&2
 exit 64
fi 

VIEWUSERFILENAME=~/.jiracurl/user--$INSTANCE$VIEW

if [ -e		      $VIEWUSERFILENAME ] ; then
	 USERFILENAME=$VIEWUSERFILENAME
else
	 USERFILENAME=~/.jiracurl/user
fi

echo $(<$USERFILENAME )
