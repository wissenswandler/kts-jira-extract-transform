shopt -s extglob

jq -sc \
'map( { s : .fields.summary , k : .key } ) | group_by( .s ) | map( { key : .[0].s , value : map( .k ) } ) | from_entries' \
issue/!(*-*) \
> index_summary_key.json
