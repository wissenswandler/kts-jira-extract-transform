#
# KTS Jira Extract-Transform
# post-installation, one-time configuration
# for new 'instance' installation
#


# boran@WIND:~/1
ln -s ../kts-jira-extract-transform/lib
ln -s ../kts-jira-extract-transform/img
ln -s ../kts-jira-extract-transform/defaults/* .

# drwxr-xr-x 1 boran users  306 Feb 10 12:45 .
# drwxr-xr-x 1 boran users  488 Feb 10 12:44 ..
# lrwxrwxrwx 1 boran users   53 Feb 10 12:45 000-header.dot -> ../kts-jira-extract-transform/defaults/000-header.dot
# lrwxrwxrwx 1 boran users   59 Feb 10 12:45 002-header-wider.dot -> ../kts-jira-extract-transform/defaults/002-header-wider.dot
# -rw-r--r-- 1 boran users 1390 Feb 10 12:44 010-header-LR.dot
# drwxr-xr-x 1 boran users   40 Feb 10 12:44 extlib
# drwxr-xr-x 1 boran users  138 Feb 10 12:44 .git
# -rw-r--r-- 1 boran users  710 Feb 10 12:44 .gitignore
# lrwxrwxrwx 1 boran users   33 Feb 10 12:45 img -> ../kts-jira-extract-transform/img
# lrwxrwxrwx 1 boran users   33 Feb 10 12:45 lib -> ../kts-jira-extract-transform/lib
# -rw-r--r-- 1 boran users  537 Feb 10 12:44 README.md
# drwxr-xr-x 1 boran users 3856 Feb 10 12:44 symwork.atlassian.net
# lrwxrwxrwx 1 boran users   21 Feb 10 12:44 symwork-test.atlassian.net -> symwork.atlassian.net
# -rw-r--r-- 1 boran users   26 Feb 10 12:44 timespent-fields.txt

#
# following step is instance-specific -> needs loop with globbing 
#

# cd symwork.atlassian.net/
# boran@WIND:~/1/symwork.atlassian.net
# 0-download-epic-summaries-by-dirname.sh
