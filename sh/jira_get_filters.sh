#
# Note: must be executed over admin account
#

jirapage.sh "filter/search?overrideSharePermissions=true&expand=jql,owner" | jq '{ name, id, jql, owner : .owner.displayName}' > filters-short.json
