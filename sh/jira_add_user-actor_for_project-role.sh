#
# ADD user with account-id $3 as actor for role with id $2 in project (id or key) $1
#
# potential error cases:
#
# HTTP 400
# {"errorMessages":["The supplied users did not contain any valid account IDs"],"errors":{}}
#
# HTTP 400
# {"errorMessages":["'<account-id>' is already a member of the project role."],"errors":{}}

INSTANCE="$(sense-web-instance.sh)"

OUTPUT_FILE=api-results.json

CURLOPTS="--silent --output $OUTPUT_FILE --write-out %{http_code}"

HTTP_CODE=$( \
curl $CURLOPTS $( jira_credentials.sh ) -X POST -H 'Content-Type: application/json' \
https://$INSTANCE/rest/api/3/project/$1/role/$2 \
--data "{  \"user\": [ \"$3\" ]  }"	\
)

if [[ ${HTTP_CODE} -lt 200 || ${HTTP_CODE} -gt 299 ]] ; then
 >&2 echo "KTS reports: HTTP Status $HTTP_CODE (an error)"
 >&2 echo "API reports: "
 >&2 cat $OUTPUT_FILE
 >&2 echo
 exit 2
else
 jq . $OUTPUT_FILE
fi
