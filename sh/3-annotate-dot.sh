dot graph.dot | dot -Txdot_json > graph-annotated.json
jq < graph-annotated.json '.objects[]|{name, "layoutY" : .pos|split(",")[1]}' | jq -s 'sort_by(.layoutY|tonumber)'
