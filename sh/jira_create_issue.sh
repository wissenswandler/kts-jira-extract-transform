#! /bin/bash
#
# Knowledge Transformation System
#
# create a Jira issue
#  in the Jira instance which is determined by the parent dir
#  in the Jira-Project whose key equals the current dir
#  with the first issuetype which is configured in that Jira-Project
#  with a Summary of first parameter
#
#  and optional JSON-Field-Code in second parameter (requires a leading comma!)
#
# returns key of new issue
#

INSTANCE=$(sense-web-instance.sh)
if [ $? -ne 0 ] ; then exit 64; fi

data="\
{ \
  \"fields\" : \
  { \
    \"summary\" :                       \"$1\"   , \
    \"project\"   : { \"key\" : \"$(basename $(pwd))\" } , \
    \"issuetype\" : { \"id\" : $(jira-get-first-creatable-issuetype.sh) } \
    $2 \
  } \
}"

curl $CURLOPTS $( jira_credentials.sh ) -X POST -H 'Content-Type: application/json' \
https://$INSTANCE/rest/api/3/issue \
--data "$data" > create-result.json
jq -r .key       create-result.json \
         > temp-issue-key.json

if  [ -e   temp-issue-key.json ] ; then
 if [ "$(< temp-issue-key.json)" == "null" ] ; then 
   rm      temp-issue-key.json 
 else
   rm            create-result.json
   mv      temp-issue-key.json              new-issue-key.json
#  firefox -new-tab "${INSTANCE}/browse/$(< new-issue-key.json)"
    cat new-issue-key.json
   exit 0 # SUCCESS
 fi
fi

# else: FAILURE

>&2 echo "Creating new issue did not work. Here is the data payload used in the attempt: "
>&2 echo " here is the result message: "
>&2 jq . create-result.json
rm       create-result.json
echo " ... and here is the data payload used in the attempt: "
echo "$data" | jq
exit 64
