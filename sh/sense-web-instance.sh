#! /bin/bash
#
# sense the dotted name either from the current dir, or from the parent dir
#

DIRTHIS=$(  basename            "$(pwd)"    )
DIRSUPER=$( basename "$(dirname "$(pwd)" )" )

if   echo $DIRTHIS  | grep -qF "." ; then
 INSTANCE=$DIRTHIS
elif echo $DIRSUPER | grep -qF "." ; then
 INSTANCE=$DIRSUPER
else
 >&2 echo "ERROR: unable to detect JIRA-INSTANCE from dotted directory (neither this nor super dir)"
 >&2 echo " and exiting now."
 exit 64
fi 

echo $INSTANCE
