if [ -z $1 ] ; then

 echo "Usage: 'touch' issue $1 by adding a label 'touch' and removing it immediately. Technically triggers two updates."
 echo
 echo " Technically triggers two updates. Second update simply serves a cleanup. Side-effect is a current 'updated' timestamp."

 exit 64
fi

INSTANCE="$(sense-web-instance.sh)"

curl $CURLOPTS $( jira_credentials.sh ) -X PUT -H 'Content-Type: application/json' \
https://$INSTANCE/rest/api/3/issue/$1 \
--data "{  \"update\" : { \"labels\" : [ { \"add\"	: \"touch\" } ] }  }"

curl $CURLOPTS $( jira_credentials.sh ) -X PUT -H 'Content-Type: application/json' \
https://$INSTANCE/rest/api/3/issue/$1 \
--data "{  \"update\" : { \"labels\" : [ { \"remove\"	: \"touch\" } ] }  }"
