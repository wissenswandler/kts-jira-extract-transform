# download icon for avatar as defined by first positional parameter ($1) and save it in img folder
# to be executed from an JIR (Jira-Instance-Directory)
#
# licensed as AGPL-3

jiracurl.sh issuetype \
| \
jq ".[]|select(.avatarId == $1).iconUrl" \
| \
while read -r url; do echo curl -s $url \> avatar--$1; done
