#
# SET (and replace all previous) group-actor $3 for role $2 in project $1
#

INSTANCE="$(sense-web-instance.sh)"

curl $CURLOPTS $( jira_credentials.sh ) -X PUT -H 'Content-Type: application/json' \
https://$INSTANCE/rest/api/3/project/$1/role/$2 \
--data "{  \"categorisedActors\": { \"atlassian-group-role-actor\": [ \"$3\" ] }  }"
