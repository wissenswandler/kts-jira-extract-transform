if [   -z $1 ] ; then
 echo -e "\033[1;31m" "error: jira_update_space.sh " "[project-id] " "[json-filename] " "\033[0m"
 exit 1
fi
if [ ! -f $2 ] ; then
 echo -e "\033[1;31m" "error: jira_update_space.sh " "[project-id] " "[json-filename] " "\033[0m"
 exit 1
fi

export CURLOPTS=-s

INSTANCE="$(sense-web-instance.sh)"

curl $CURLOPTS $( jira_credentials.sh ) -X PUT -H 'Content-Type: application/json' \
 --url https://$INSTANCE/rest/api/3/project/$1	\
 --data @$2			\
 > space_receipt.json

if [ $? == 0 ] ; then
 echo -e "\033[1;32m" space updated."\033[0m"
else
 echo -e "\033[1;31m" cURL error with payload: "\033[0m"
 jq . $2
fi
