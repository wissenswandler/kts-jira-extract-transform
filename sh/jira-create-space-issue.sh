#! /bin/bash
#
# Knowledge Transformation System
#
# create a Jira SPACE issue
#  in the Jira instance which is determined by the parent dir
#  in the Jira-Project whose key equals the current dir
#  with the first issuetype which is configured in that Jira-Project
#  with a Summary AND SPACE (by key) of first parameter
#

INSTANCE=$(sense-web-instance.sh)
if [ $? -ne 0 ] ; then exit 64; fi

if [ ! -z "$2" ] ; then
 descriptionClause="\"description\":\
 { \
  \"version\": 1, \
  \"type\": \"doc\", \
  \"content\": [ \
    { \
      \"type\": \"paragraph\", \
      \"content\": [ \
        { \
          \"type\": \"text\", \
          \"text\": \"$2\" \
        } \
      ] \
    } \
  ] \
 } , "
fi

data="\
{ \
  \"fields\" : \
  { \
    \"summary\" :                       \"$1\"   , \
    \"customfield_10100\" : { \"key\" : \"$1\" } , \
    $descriptionClause \
    \"project\"   : { \"key\" : \"$(basename $(pwd))\" } , \
    \"issuetype\" : { \"id\" : $(jira-get-first-creatable-issuetype.sh) } \
  } \
}"

rm -f temp-issue-key.json

curl $CURLOPTS $( jira_credentials.sh ) -X POST -H 'Content-Type: application/json' \
https://$INSTANCE/rest/api/3/issue \
--data "$data" > create-result.json
jq -r .key       create-result.json \
         > temp-issue-key.json

if  [ -e   temp-issue-key.json ] ; then
 if [ "$(< temp-issue-key.json)" == "null" ] ; then 
   rm      temp-issue-key.json 
 else
   rm            create-result.json
   mv      temp-issue-key.json              new-issue-key.json
#  firefox -new-tab "${INSTANCE}/browse/$(< new-issue-key.json)"
   echo "new issue key: "               $(< new-issue-key.json)
   exit 0 # SUCCESS
 fi
fi

# else: FAILURE

>&2 echo "Creating new issue did not work. Here is the data payload used in the attempt: "
>&2 echo " here is the result message: "
>&2 jq . create-result.json
rm       create-result.json
>&2 echo " here is the data payload used in the attempt: "
>&2 echo "$data"
exit 64
