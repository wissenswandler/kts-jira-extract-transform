# REST by JQL
jq -s 'map( .key ) | unique' issue-lines.json > keys-from-REST.json

# RDF by class (issuetype)
rdfcurl.sh "select ?k {?s a/rdfs:label \"$1\" . ?s jira:key ?k}" | jq -s 'map( .k ) | unique' > keys-from-RDF.json

# RDF by space (project)
#rdfcurl.sh 'select ?k {?s jira:project/jira:key "SYMMETA" . ?s jira:key ?k}' | jq -s 'map( .k ) | unique' > keys-from-RDF.json


diff keys-from-REST.json keys-from-RDF.json
