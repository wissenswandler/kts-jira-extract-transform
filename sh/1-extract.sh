#
# extract issue lines from Jira
#
# licensed under AGPLv3
# by Boran Gögetap (Wissenswandler)
#

ALLRESULTSFILE=issue-lines.json

if [ "$1" == "-f" ] ; then

  rm -f $ALLRESULTSFILE
  echo -e "\033[1;31mforcing extraction...\033[0m"
  shift 1

fi

if [[ $# -eq 0 ]] ; then

#
# construct JQl for the desired issues
#
if test -f jql.txt; then
 jql="$(<jql.txt)"
else
 jql="project=${PWD##*/}+OR+labels=view--${PWD##*/}"
fi

#
# load specific fields, if existent for this view, or use generic field set otherwise
#
dotfields="summary,description,issuetype,parent,project,subtasks,status,issuelinks,updated"

if   test -f        ../autosum-field.txt; then
 autosumfield=",$( <../autosum-field.txt)"
fi

if   test -f     fields.txt; then
 fields="$(     <fields.txt)${autosumfield}"
elif test -f morefields.txt; then
 fields="$dotfields,$(<morefields.txt)${autosumfield}"
else
 fields="${dotfields}${autosumfield}"
fi

#
# resource saver: check for updates in Jira since last extraction
# if no such updates then exit with status 304 (not modified since) which appears as 48 (mod 256)
#
if test -f $ALLRESULTSFILE ; then
 testjql="("$jql")+and+updated+>+\"$( date -r $ALLRESULTSFILE '+%Y-%m-%d+%H:%M' )\""
 echo -e "testing \033[1;33m ${PWD##*/}\033[0m"
 jiracurl.sh "search?jql=$testjql&maxResults=0" > results.json
 if test $? -eq 0; then
  if test $( jq ".total" results.json ) -eq 0; then rm results.json; exit 304; fi
 fi # end successful curl
fi  # end test for timestamp

PAGINGQUERY="search?jql=$jql&fields=$fields"

else # at least one positional parameter

PAGINGQUERY=$1
 
fi # zero positional parameter

#
# perform actual extraction
#
rm -f      $ALLRESULTSFILE
touch      $ALLRESULTSFILE
touch      timestamp-before-extraction
echo "1" >       total.json
startAt="0"

echo -e "getting \033[1;33m" $PAGINGQUERY "\033[0m"

while test $( wc -l < $ALLRESULTSFILE ) -lt $(<total.json)
do

 jiracurl.sh "$PAGINGQUERY&maxResults=100&startAt=$startAt" > results.json
 if test $? -ne 0; then 
   rm -f      $ALLRESULTSFILE
   rm -f timestamp-before-extraction 
   rm -f      results.json
   rm -f        total.json
   echo -e "\033[1;31mKTS aborting.\033[0m"
   exit 304
 fi # end failed jiracurl

 if test $( jq ".total" results.json ) -eq 0; then rm results.json; exit 304; fi
 jq    < results.json " .total "    >  total.json
 jq -c < results.json '.[]|arrays|.[]'  >> $ALLRESULTSFILE
 startAt=$[$startAt + $( jq .maxResults results.json )]
done

echo -e "done extracting \033[1;33m" $(<total.json) "\033[0m issue lines"

touch -d @$(stat -c "%Y" timestamp-before-extraction ) $ALLRESULTSFILE
rm timestamp-before-extraction 
rm       results.json
rm         total.json
