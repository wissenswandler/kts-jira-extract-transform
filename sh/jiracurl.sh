if   [ -z $1 ] ; then

 echo "Usage: jiracurl.sh [JIRA-INSTANCE] REQUEST"
 echo " where JIRA-INSTANCE can be an explicit (fully qualified) hostname, or 'self', or 'super' to read from the current or super directory"

else


if [ -z $HTTP_REQUEST_METHOD ] ; then
 METHODCLAUSE=""
else
 METHODCLAUSE="-X $HTTP_REQUEST_METHOD"
fi


if [ -z $2 ] ; then

 REQUEST=$1
 DIRTHIS=$(  basename            "$(pwd)"    )
 DIRSUPER=$( basename "$(dirname "$(pwd)" )" )

 if   echo $DIRTHIS  | grep -qF "." ; then
  INSTANCE=$DIRTHIS
 elif echo $DIRSUPER | grep -qF "." ; then
  INSTANCE=$DIRSUPER
  VIEW="--"$DIRTHIS
 else
  echo "ERROR: unable to detect JIRA-INSTANCE from dotted directory (neither this nor super dir)"
  echo " and exiting now."
  exit 64
 fi 

else # two parameters

 REQUEST=$2
 INSTANCE=$1

fi   # one / two parameters

if [[ $REQUEST == http* ]] ; then
 REQURL=$REQUEST
elif [[ $REQUEST == /* ]] ; then
 REQURL="https://$INSTANCE$REQUEST"
else
 REQURL="https://$INSTANCE/rest/api/latest/$REQUEST"
fi

# echo curl --fail --globoff $CURLOPTS $( jira_credentials.sh ) $METHODCLAUSE $REQURL >&2
       curl --fail --globoff $CURLOPTS $( jira_credentials.sh ) $METHODCLAUSE $REQURL

fi # Usage
