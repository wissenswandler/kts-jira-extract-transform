#
# page through Jira's paged results of initial request for $1,
# sending accumulated elements of values[] to stdout
#

get_one_page()
{
  >&2 echo getting $request ...
  temp=$( jiracurl.sh "$request" )
  echo $temp | jq .values[]
  request=$( echo $temp | jq -r .nextPage )
}

request=$1

get_one_page
while [ $( echo $temp | jq -r .isLast  ) = "false" ] 
do
	get_one_page
done
>&2 echo ... done.
