#
# KTS rdfcurl
#

endpoint=http://localhost:8080/r/repositories/webchunks

OUTPUT_FILE=sparql-results.json

CURLOPTS="--silent --output $OUTPUT_FILE --write-out %{http_code}"

# this is available only in curl 7.76 or later - currently not on KGO
# CURLOPTS=--fail-with-body

#
# test payload source
#
if [ -f "$1" ]; then
#
# query by file
#
HTTP_CODE=$( curl	$CURLOPTS			\
	-H 'accept: application/sparql-results+json'	\
	-H 'Content-Type: application/sparql-query'	\
	--data-binary "@$1"				\
	$endpoint					\
)
else
#
# query by parameter
#
set -o noglob
if [ -e prefix_.sparql ] ; then
 prefixed_query=$(cat    prefix_.sparql <(echo $1))
elif [ -e ../prefix_.sparql ] ; then
 prefixed_query=$(cat ../prefix_.sparql <(echo $1))
else
 prefixed_query=$1
fi

 >&2 echo "prefixed_query = " $prefixed_query

if [ -e postfix_.sparql ] ; then
 postfixed_query=$(cat     <(echo $prefixed_query) postfix_.sparql)
else
 postfixed_query=$prefixed_query
fi

HTTP_CODE=$( curl	$CURLOPTS			\
	-H 'accept: application/sparql-results+json'	\
	--data-urlencode "query=$postfixed_query"	\
	$endpoint					\
)
fi # end payload source


if [[ ${HTTP_CODE} -lt 200 || ${HTTP_CODE} -gt 299 ]] ; then
 >&2 echo "KTS reports: HTTP Status $HTTP_CODE (an error)"
 >&2 echo "SPARQL endpoint reports: "
 >&2 cat $OUTPUT_FILE
 >&2 echo
 exit 2
else
 jqr.sh $OUTPUT_FILE
fi
