#
# KTS
#
# create (view) dir -if not existent-
# with dir name from $1 and title from all following positional parameters $2 .. $z
# on same filesystem level as PWD, i.e. new dir will be at ../newdir
#

if [ -d "../$1" ]; then
 echo "   " "$1" haben wir schon, keine Aktion
else
 projectkey=$1
 shift
 echo "erzeuge neue View für: " $projectkey "(" $@ ")"
 mkdir ../$projectkey
 echo "$@" "(automatische View per Jira-Projekt)" > ../$projectkey/title.txt
fi
