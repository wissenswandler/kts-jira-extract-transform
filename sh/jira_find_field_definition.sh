if [ ! -f field_s.json ] ; then echo ERROR no file field_s.json - please run jira_get_fields.sh; exit 404; fi

jq "map(  select( [ .name , .key ] + .clauseNames | .[] | test(\"$1\";\"i\") )  ) | unique" field_s.json
