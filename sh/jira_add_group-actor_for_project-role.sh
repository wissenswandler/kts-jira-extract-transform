#
# ADD group with name $3 as actor for role with id $2 in project (id or key) $1
#
# with error handling based on HTTP return code: any HTTP return code < 200 or > 209 will cause unix exit code = 2
# so that this script can safely be chained with another command only in case of success
# e.g. for a "add / remove"-transaction that shall only remove if the previous add has suceeded
#
# potential error cases are:
#
# HTTP 400
# {"errorMessages":["'Test-User' is already a member of the project role."],"errors":{}}
#
# HTTP 404
# {"errorMessages":["We can't find 'Test-Use' in any accessible user directory. Check they're still active and available, and try again."],"errors":{}}
#

INSTANCE="$(sense-web-instance.sh)"

OUTPUT_FILE=api-results.json

CURLOPTS="--silent --output $OUTPUT_FILE --write-out %{http_code}"

HTTP_CODE=$(  curl $CURLOPTS $( jira_credentials.sh ) -X POST -H 'Content-Type: application/json' \
https://$INSTANCE/rest/api/3/project/$1/role/$2 \
--data "{  \"group\": [ \"$3\" ]  }"  )

if [[ ${HTTP_CODE} -lt 200 || ${HTTP_CODE} -gt 299 ]] ; then
 >&2 echo "KTS reports: HTTP Status $HTTP_CODE (an error)"
 >&2 echo "API reports: "
 >&2 cat $OUTPUT_FILE
 >&2 echo
 exit 2
else
 jq . $OUTPUT_FILE
fi
