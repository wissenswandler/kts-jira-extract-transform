if test ! -e "$1" ; then mkdir $1; fi

if test   -d "$1" ; then

	echo -e "entring \033[1;33m $(basename $1)\033[0m"
  pushd $1 > /dev/null

  123js.sh

  popd > /dev/null

  ln -sf $1/issues.json $1.json
  ln -sf $1/graph.svg   $1.svg

fi # dir exists
