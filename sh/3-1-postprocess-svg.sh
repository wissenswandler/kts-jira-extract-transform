#   Copyright 2020 Boran Gögetap <- boran@goegetap.name
: <<'END'
    build SVG from DOT, for interactive SVG graph viewer,
    ... Teil von // part of ...
    Knowledge Transformation System (KTS)

    KTS ist Freie Software: Sie können es unter den Bedingungen
    der GNU Affero General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiter verteilen und/oder modifizieren.

    KTS wird in der Hoffnung, dass es nützlich sein wird, aber
    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU Affero General Public License für weitere Details.

    Sie sollten eine Kopie der GNU Affero General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

    //

    KTS is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KTS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with KTS.  If not, see <https://www.gnu.org/licenses/>.
END

#LIB='https://wissenswandler.github.io/1.0.0/'
#wget -q --spider ${LIB}graph.js
#if [ $? -eq 0 ]; then
#  echo "using js/css LIB $LIB"
#else
  echo "using js/css LIB offline "
  LIB="../lib/"
#fi

mv $1 temp.svg
cat   temp.svg | \

sed "1 a <?xml-stylesheet type=\"text/css\" href=\"${LIB}graph.css\"?>" \
| \
sed "9 a <script xlink:href=\"${LIB}graph.js\" />" \
|
sed 's/<svg\s*\(width=".*"\sheight=".*"\)/<!--\1-->\n<svg /' \
|
sed 's/\(viewBox=".*"\) \(xmlns=.*$\)/\2\n<!--\1-->/' \
|
sed 's/<\/svg>/<foreignObject id="fo0" width="100%" height="100%" display="none"><div id="htmldiv" xmlns="http:\/\/www.w3.org\/1999\/xhtml" \/><\/foreignObject><\/svg>/' \
> $1
rm temp.svg


# don't set width to 100% because it makes SVG embedded in HTML impossible to zoom (will always fit the window)
#| \
#sed 's/<svg width=".*" height=".*"/<svg width="100%" /' \
