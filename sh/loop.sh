export CURLOPTS=-s

#echo "(dollar-1 with globbing)" $1 "(end)"
set -f # disables file name generation ("path name expansion"); is equivalent to setting the noglob shell option
#echo "(dollar-1 w/o  globbing)" $1 "(end)"

if [[ $1 == 1 ]] ; then
exit_after_one_loop=true
searchpattern=$2
else
exit_after_one_loop=false
searchpattern=$*
fi

if [[ $searchpattern == "" ]] ; then searchpattern='*' ; fi

export NoGos="-not -name img -not -name '_*' -not -name '.*' -not -name ."

while true
do

 export           searchcommand="find . -maxdepth 1 -type d $NoGos -iname $searchpattern"
 loopdirs=$(eval $searchcommand)
 if [[ $loopdirs == "" ]] ; then
  echo 
  echo -e "\033[1;31mError: script parameter did not evaluate into a list of directories, or caused an error in find.\033[0m"
  echo Here is the resulting internal search command:
  echo 
  echo -e "\033[1;33m $searchcommand\033[0m"
  echo 
  echo KTS aborting.
  exit 404
 fi
 
 for dir in $loopdirs; do

  if [[ ! -f $dir/robots.txt ]] ; then
 
   cd12.sh $dir

  fi

 done #for

 echo "One loop completed."
 
 if $exit_after_one_loop ; then exit; fi

 sleep 1
done #while almost-endless loop
