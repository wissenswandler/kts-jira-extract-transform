#
# REPLACE user by id $3 with group by name $4 as actor from role with id $2 in project (id or key) $1
# 
# finally lists all actors for role $2 in project $1
#
# removal only succeeds if addition had also succeeded
#
# addition succeeds also if removal was not successful (e.g. because user did not play an actor role or because user does not exist)
#

INSTANCE="$(sense-web-instance.sh)"

jira_add_group-actor_for_project-role.sh             $1 $2 "$4" > /dev/null && \
jira_remove_user-or-group-actor_from_project-role.sh $1 $2 user=$3

jiracurl.sh project/$1/role/$2 | jq
