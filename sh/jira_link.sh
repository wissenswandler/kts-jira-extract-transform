#! /bin/bash
#
# Knowledge Transformation System
#
# link two Jira issues in cause-to-effect order
#  in the Jira instance which is determined by the parent dir
#

INSTANCE=$(sense-web-instance.sh)
if [ $? -ne 0 ] ; then exit 64; fi

if [ -z $2 ] ; then
 echo Usage:
 echo " [1] $(basename $0) outward-key linktype-key inward-key"
 echo " [2] $(basename $0)             linktype-key inward-key"
 echo "(in form [2], the outward key is read from most recently created issue)"
 exit 64
fi

if [ -z $3 ] ; then
 out=$(< new-issue-key.json)
else
 out=$1
 shift
fi

typ=$1
inw=$2

curl $CURLOPTS $( jira_credentials.sh ) -X POST -H 'Content-Type: application/json' \
https://$INSTANCE/rest/api/3/issueLink \
--data "{  \"outwardIssue\": { \"key\": \"$out\" }, \"inwardIssue\": { \"key\": \"$inw\" },  \"type\": { \"id\": \"$typ\" }  }"
