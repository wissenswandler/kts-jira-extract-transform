#
# REMOVE user/group by id/name $3 as actor from role with id $2 in project (id or key) $1
#
# see valid forms of user/group identification in Jira REST API: 
# https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-project-role-actors/#api-rest-api-3-project-projectidorkey-role-id-delete
#
# user -- The user account ID of the user to remove from the project role.
#
# group -- The name of the group to remove from the project role. This parameter cannot be used with the groupId parameter. As a group's name can change, use of groupId is recommended.
#
# groupId -- The ID of the group to remove from the project role. This parameter cannot be used with the group parameter.



INSTANCE="$(sense-web-instance.sh)"

curl $CURLOPTS $( jira_credentials.sh ) -X DELETE -H 'Content-Type: application/json' \
"https://$INSTANCE/rest/api/3/project/$1/role/$2?$3"
