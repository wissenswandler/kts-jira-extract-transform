def doit:

[ . += {selectiondistance : 0} ]
+
if( $includeLinkedNodes == "all" or $includeLinkedNodes == "supports"	) then 
[ .fields.issuelinks[].inwardIssue  // empty ] else [] end
+
if( $includeLinkedNodes == "all" or $includeLinkedNodes == "depends"	) then 
[ .fields.issuelinks[].outwardIssue // empty ] else [] end
|
.[]
|
.key as $key
|
{
 key
 ,
 self
 ,
 selectiondistance
 ,
 fields : .fields |
 {
  summary
  ,
  issuetype : .issuetype |
  {
   id
   ,
   name
   ,
   avatarId
  }
  ,
  status    : .status |
  {
   name
   ,
   statusCategory : .statusCategory | { id }
  }

  #
  # following fields are only included with top-level issues
  # - not with linked issues!
  #
  ,
  description
  ,
  labels

  #
  # synthesized field parentgroup, for generic grouping
  #
  ,
  parentgroup :
  { 
   key     : ( .parent.key // $key )
   ,
   summary : ( .parent.fields.summary // .summary )
  }  

  # specific to each Jira installation
  # only available with Jira-Software license!
  ,
  epiclink  : (if keys|contains([$epicLinkField]) then to_entries[]|select( .key == $epicLinkField ).value else null end)
  
 }
}

;
