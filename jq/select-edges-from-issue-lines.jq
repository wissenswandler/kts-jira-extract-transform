# normalize each issuelink following the outward direction into an s-p-o triple,
# with s and o being of issue type
# and p the link's type

def doit:
. as $root | .fields.issuelinks[] |
 if  .inwardIssue then ( { o : $root|del( .fields.issuelinks), s :  .inwardIssue, p : .type, id, self } ) else empty end
 , 
 if .outwardIssue then ( { s : $root|del( .fields.issuelinks), o : .outwardIssue, p : .type, id, self } ) else empty end
;
