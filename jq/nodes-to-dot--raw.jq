#
# input: array of arrays
#      = epics of issues
#  -> clusters of nodes
#
# with the first array holding all isues without an epic link,
# which we render as nodes without a containing cluster

#
# visibility switches
#

($show.typename or $show.statusname or $show.key) as $showBottom
|
($show.icon or $show.attributes or $show.epic or $showBottom | not) as $showLabelOnly
|
. as $root
|
( length > 1 ) as $useClusters

#| ($spo | group_by( .s.key )|map({ s : .[0].s.key, count : length })) as $subject_count
#| ($spo | group_by( .o.key )|map({ o : .[0].o.key, count : length })) as  $object_count
|
#
# loop over clusters
#
(

#
# dynamic prologue
#
#( .[0] | ( .[0].self|split("/rest/api/")[0] ) ) as $selfbase # base URL of all values for .self key
#|
#"node [HREF=\"\($selfbase)/browse/\\N\""
#,

 .[]
 | .[0].fields.epiclink   as $epic
 | .[0].fields.parentgroup.key as $parent
 |
 if $useClusters and $epic then 
  (  $epicSummaryByKey[] | select( .key == $epic ) | .summary ) as $epicSummary
  |
  ( .[0].self|split("/rest/api/")[0] ) as $selfbase
  |
    "subgraph <cluster_\($epic)> {"
  , " label=\"\( $epicSummary // "?" )\""
  , " URL=\"\($selfbase)/browse/\($epic)\""
 elif $useClusters and $parent then 
  ( .[0].fields.parentgroup.summary ) as $epicSummary
  |
  ( .[0].self|split("/rest/api/")[0] ) as $selfbase
  |
    "subgraph <cluster_\($parent)> {"
  , " label=\"\( $epicSummary // "?" )\""
  , " URL=\"\($selfbase)/browse/\($parent)\""
 else
  empty 
 end
 ,
#
# loop over nodes within cluster
#
 (
   .[]
   | . as $this
   | ( .self|split("/rest/api/")[0] ) as $selfbase
   | .fields.issuetype.name 	as $typename
   | .fields.issuetype.id	as $typeid
   | ( ($this.fields.description|..|.text?) // $this.fields.description // " " | gsub( "\""; "\\\"" )? ) as $descriptiontext
   | (if ($descriptiontext|length > 1000) then ($descriptiontext[:1000] + " [...]") else $descriptiontext end) as $description
   | (.key|contains("META")) as $meta
   | (if $meta then "filled" else "rounded,filled" end) as $basestyle
   | (if ($spo | map( select( (.s.key == $this.key) and (.p.name|startswith("-")) ) )  ) | length > 0 then true else false end) as $impacted
   | (if ($spo | map( select( (.o.key == $this.key) and (.p.name|startswith("-")) ) )  ) | length > 0 then true else false end) as $causing
  | (
      $spo | map( select( (.s.key == $this.key) and ( (.p.name|startswith("RACI ")) or .o as $o
  | ( $root[][]|select( .key == $o.key )|.fields.issuetype.name=="Feld" ) ) ) )
    ) as $attributes
  | ( $spo | map( select( (.s.key == $this.key) and (.p.name|startswith("1 Nachfolge")) ) ) ) as $predecessors
  | ( $spo | map( select( (.o.key == $this.key) and (.p.name|startswith("1 Nachfolge")) ) ) ) as $ancestors
  | (
     ( $predecessors|length == 1 )
     and
     (    $ancestors|length == 1 )
     and
     $predecessors[0].o as $o
     |
     (
      $root[] | map( select( .key == $o.key ) )[0]
      |
      (
       (.fields.issuetype == $this.fields.issuetype) 
       and
       (.fields.summary   == $this.fields.summary)
      ) 
     )
     and
     $ancestors[0].s as $o
     |
     (
      $root[] | map( select( .key == $o.key ) )[0]
      |
      (
       (.fields.summary   == $this.fields.summary)
      ) 
     )
   ) as $sameAsBefore
   |
   "HREF=\"\($selfbase)/issues/?jql=type=\($typeid)+ORDER+BY+summary\"" as $typeref
   |
   (if ($show.epic|not) or $useClusters or $this.fields.epiclink==null then false else true end) as $showEpic
   |
   "<\(.key)> [URL=\"\($selfbase)/browse/\(.key)\""
 , " tooltip=\"\( $description )\""
 ,
 if $sameAsBefore 
 then
#
# simple case: only show a tiny dot as shape
#
 " shape=point"
 elif .fields.issuetype.name | ascii_downcase ==  "operation"
 then
#
# simple case: only show a circle with (hopefully short) label inside
#
 " label=<" + ( .fields.summary|@html ) + "> shape=circle"
 elif $showLabelOnly
 then
#
# simple case: only show the label - no need for complex table
#
 " label=<" + ( .fields.summary|@html ) + ">"
 else
#
# standard case: potentially complex, HTML-Table-like label
#
 (
   " label=<"
 , "<TABLE BORDER=\"0\" CELLSPACING=\"0\">"
 , " <TR>"
 , if $show.icon then
   "  <TD " + if $showEpic then "ROWSPAN=\"2\" " else "WIDTH=\"18\" HEIGHT=\"18\" FIXEDSIZE=\"TRUE\" CELLPADDING=\"0\" VALIGN=\"TOP\" " end + $typeref + ">"
    , "<IMG SRC=\"../img/avatar--\(.fields.issuetype.avatarId|tostring).png\" />"
    ,"</TD>"
   else
    empty 
   end
 , "  <TD COLSPAN='" + if $show.icon then "3" else "5" end + "'><B>\(.fields.summary|@html)</B></TD></TR>"
 , if $showEpic then
   " <TR><TD> </TD><TD COLSPAN='2' BGCOLOR='\(
                   $colors[] | select( .label == 
      (  $epicSummaryByKey[] | select( .key == $this.fields.epiclink ) | .color  )
                                                                     ) | .background 
    )'><FONT POINT-SIZE='11' COLOR='\(
                   $colors[] | select( .label == 
      (  $epicSummaryByKey[] | select( .key == $this.fields.epiclink ) | .color  )
                                                                     ) | .foreground 
    )'>\(
         $epicSummaryByKey[] | select( .key == $this.fields.epiclink ) | .name   )</FONT></TD></TR>"
   else empty end
 ,
if $show.attributes then (
 $attributes[]
 |
 . as  $a
 |
 if
   (
    ($spo | map(select( .s.key == $a.o.key )) | length)
    +
    ($spo | map(select( .o.key == $a.o.key )) | length)
   ) == 1
 then
 $root[][]|select( .key == $a.o.key ) as $ao
 |
   " <TR><TD>\(if ($ao.fields.issuetype.name == "Feld") then "F" else $a.p.name[-1:] end)</TD>"
 + "  <TD><IMG SRC=\"../img/avatar--\($ao.fields.issuetype.avatarId|tostring).png\"/></TD>"
 + "  <TD ALIGN=\"LEFT\">\( $ao.fields.summary|@html )</TD></TR>"
 else empty end
) else empty end # showAttributes
 ,
if $showBottom then (
 " <TR>"
 , 
 "<TD \($typeref) COLSPAN=\"2\" SIDES=\"LBR\" ALIGN=\"LEFT\">"
 ,  "<I><FONT POINT-SIZE='8'>" , $typename ,           "</FONT></I></TD>"

 , "<TD><FONT POINT-SIZE='8'>" , .fields.status.name , "</FONT></TD>"

 , "<TD ALIGN='RIGHT'>"
 ,     "<FONT POINT-SIZE='8'>" , .key ,                "</FONT></TD>"

 , "</TR>"
) else empty end
 , "</TABLE>>"
#
# shape by project and type
#
,
if ["Feld","Status"] | contains( [$typename] )
then
 " shape=none style=\"\""
else
 empty
end

#
# style by status
#
 
,if $this.fields.status.statusCategory.id == 4 then " style=\"\($basestyle)\""        else empty end
,if $this.fields.status.statusCategory.id == 2 then " style=\"\($basestyle),dotted" +
  if $impacted or $causing then ",bold" else "" end + "\""                            else empty end
,if $this.fields.status.statusCategory.id == 3 then " style=\"\($basestyle),dashed\"" else empty end

#
# color by reasoning
#
,
if  $impacted and $causing then " colorscheme=ylorrd5 color=4" else
 if $impacted              then " colorscheme=ylorrd5 color=3" else
  if              $causing then " colorscheme=ylorrd5 color=5" else empty end end
end

) end # sameAsBefore

, " ]"
) # end node
 ,
 if $useClusters and ($epic | $parent) then " }" else empty end

) # end clusters
#,
#"node [shape=plain\"]"
