def doit:

. as $root
|
map( $nodes[][][].key+"/" ) as $nodesKeys
|
( if .[0] then .[0].s.self|split("/rest/api/")[0] else "" end ) as $selfbase
|
( "▲" ) as $arrowUp
|
( "▼" ) as $arrowDn
|
.[]
|
.o.key as $okey
|
.s.key as $skey
|
select	# only those edges that connect visible nodes
(
 (  $nodesKeys | contains( [ $skey+"/" , $okey+"/" ] )  )
)
|
( [.s.fields.issuetype.name] ) as $sTypenameArray
|
( [.o.fields.issuetype.name] ) as $oTypenameArray
|
select
(
 (  $unwantedTypenames|contains( $sTypenameArray )|not  )
 and
 (  $unwantedTypenames|contains( $oTypenameArray )|not  )
)
|
( .p.name|split("-- style: ")[1] ) as $style
|
(  .p.inward|rtrimstr($arrowUp) ) as  $inwardLabel
|
( .p.outward|rtrimstr($arrowDn) ) as $outwardLabel
|
( $style|not ) as $renderlabel
|
("\(.o.fields.summary) → \($inwardLabel) → \(.s.fields.summary) → \($outwardLabel) ↩" | gsub( "\""; "\\\"" )? // " ") as $tooltip
|
(
if
(
  ( .p.name|startswith(  "2") )
  or
  ( .p.name|startswith( "-2") )  
)
then
 "<\(.s.key)> -> <\(.o.key)>"
else
 "<\(.o.key)> -> <\(.s.key)>"
end
)
+
" ["
#
# tooltip (always)
#
+
" labeltooltip=\"\($tooltip)\""
+    " tooltip=\"\($tooltip)\""

#
# hyperlinks (always) shows both linked node in issue navigator
#
+
  " href=\"\($selfbase)/issues/?jql=key+in+(\(.s.key),\(.o.key))\""

#
# labeling (conditional)
#
+ if $renderlabel then " label=\"\($inwardLabel)\"" else "" end

#
# arrowhead, arrowtail, style (per type)
#
+ " \($style//"")"

#
# color by reasoning
#
+ if ( .p.name|startswith("-") )	# negative value stream, color orange
  then
  " colorscheme=ylorrd5 color=4"
  else
    if ( .p.name|startswith("0") )	# neutral value stram, color grey
    then
    " color=grey"
    else
    ""
    end
  end

#
# no 'head' if pointing to Operation
#
+ if ( .s.fields.issuetype.name | ascii_downcase == "operation" ) then " dir=none" else "" end


#
# end
#
,
" ]"

;
