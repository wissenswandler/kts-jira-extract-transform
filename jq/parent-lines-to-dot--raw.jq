# render subtask containments to DOT
# IF parent is part of this view

map( $nodes[][][]|.key+"/" ) as $nodesKeys
|
select(
 .fields.parent 
)
|
.fields.parent.key as $parentkey
|
.key as $thiskey
|
select  # only those edges that connect visible nodes
(
 (  $nodesKeys | contains( [ $thiskey+"/", $parentkey+"/" ] )  )
)
|
"<" + $thiskey + "> -> <" + $parentkey + "> [ arrowhead=diamond arrowtail=none ]"
