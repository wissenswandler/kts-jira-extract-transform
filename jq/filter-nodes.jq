def doit:

true as $showAttributes
|
sort_by( .key, .fields.description, .fields.epiclink )
|
reverse
|
unique_by( .key )
|
map(
 select
 (
  . as $this
  |
  [ .fields.issuetype.name ] as $thisTypenameArray
  |
  [ .fields.epiclink ] as $thisEpiclinkArray
  |
  (
   (  $unwantedTypenames|contains( $thisTypenameArray )|not  )
   and
   (  ($wantedTypenames|length == 0) or (  $wantedTypenames|contains( $thisTypenameArray )  )   )?
   and
   (  ($wantedEpics    |length == 0) or (  $wantedEpics    |contains( $thisEpiclinkArray )  )   )?
   and
( ( ( (
  (
   $spo | map( select( .o.key == $this.key and (.p.name|startswith("RACI ")) ) ) | length > 0
  )
  or
  .fields.issuetype.name == "Feld"
 )
 and
#
# has exactly one link
#
 (
  ($spo | map( select( .s.key == $this.key )) | length) +
  ($spo | map( select( .o.key == $this.key )) | length) == 1
 )
 and
#
# has no link to node of type "Operation"
#
 (
  ($spo | map( select( (.s.key == $this.key) and (.o.fields.issuetype.name == "Operation") )) | length) +
  ($spo | map( select( (.o.key == $this.key) and (.s.fields.issuetype.name == "Operation") )) | length) == 0
 )
)
|not
) or $showAttributes)

  )
 )
)

;
