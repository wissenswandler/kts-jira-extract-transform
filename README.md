# KTS JET Core

Knowledge Transformation System, Jira Extract & Transform, Core module

licensed under AGPLv3
by Boran G�getap (Wissenswandler)

## Setup

include **/sh** in PATH

### Linux package dependencies

    curl
    jq
    graphviz
    imagemagick

### Authentication

Die Daten zur Authentication liegen in **~/.jiracurl/user** im Format z.B. ***-u user@domain:APITOKEN*** (verwendet vom Skript jiracurl.sh).  
API Tokens f�r den eigenen User gibt es hier: https://id.atlassian.com/manage-profile/security/api-tokens

### Deployment / Hosting

entweder die _Quell-Dom�nen-Stufe_ als DocumentRoot �ber einen HTTP-Server freigeben,  
oder die KTS-Build-Artefakte (\*.PNG, \*.SVG, \*.PDF, \*.HTML) in eine gew�nschte DocumentRoot kopieren,  
oder die _Quell-Dom�nen-Stufe_ als Input �ber Jekyll nach **\_site** generieren und dort freigeben.


## Operation

Vorbereitend sollten die Epic-Informationen auf der _Quell-Dom�nen-Stufe_ **(yourdomain).atlassian.net**  
mit dem Script **0-download-epic-summaries-by-dirname.sh** gecacht werden.


Die Diagramm-Erzeugung l�uft in den Arbeitsverzeichnissen auf der _View-Stufe_, zwei Ebenen unter der Wurzel, z.B. **META**,  
mit dem Script **12.sh** einmalig, oder **12-loop.sh** in einer Endlosschleife.  
KTS erzeugt dort einige PNG- SVG- und PDF-Dateien.

**Alle** Views in einer Schleife erzeugt das Script **loop.sh * ** auf der _Quell-Dom�nen-Stufe_ **(yourdomain).atlassian.net**.

### optional control files per View

note: all control files are optional. KTS JET will render diagrams even in a completely empty View folder, using conventions to query reasonable facts from Jira, and defaults for visual presentation

#### 0??-header*.dot

preambel in DOT language

all files that match above pattern will be used

defaults to: 000-header.dot in the instance container

used by: sh/2-issues2dot.sh

#### show.json

controls the visual details per node, with an optional key per detail

{
 "icon"		: true
 ,
 "attributes"	: true
 ,
 "typename"	: true
 ,
 "statusname"	: true
 ,
 "key"		: true
}

missing key defaults to value == **false**.

missing file defaults to show-all.json in instance container (all keys == **true**)
